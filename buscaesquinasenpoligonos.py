# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BuscaEsquinasEnPoligonos
                                 A QGIS plugin
 Detecta las esquinas interiores de un polígono
                              -------------------
        begin                : 2014-10-17
        copyright            : (C) 2014 by J.L.G. Pallero
        email                : jgpallero@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from buscaesquinasenpoligonosdialog import BuscaEsquinasEnPoligonosDialog
import os.path
################################################################################
################################################################################
#cargamos bibliotecas y funciones auxiliares
from qgis.gui import *
from os import sep,linesep
from math import pi
from calcesquinas import *
import time
import qgis
################################################################################
################################################################################
################################################################################
class BuscaEsquinasEnPoligonos:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'buscaesquinasenpoligonos_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = BuscaEsquinasEnPoligonosDialog()

        ########################################################################
        #LA CONEXIÓN DEL BOTÓN PARA SELECCIONAR EL NOMBRE DE LOS FICHEROS DE
        #SALIDA TIENE QUE ESTAR AQUÍ
        ########################################################################
        #selección de fichero de trabajo
        self.dlg.connect(self.dlg.pushRuta,SIGNAL("clicked()"),
                         lambda arg=self:SeleccionaFichero(arg))
        ########################################################################
        ########################################################################
        ########################################################################

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/buscaesquinasenpoligonos/icon.png"),
            u"BuscaEsquinasEnPoligonos", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&BuscaEsquinasEnPoligonos", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&BuscaEsquinasEnPoligonos", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # show the dialog
        self.dlg.show()
        ########################################################################
        ###########################COMIENZA MI CÓDIGO###########################
        ########################################################################
        #EL PROCESADO DE MULTIPLOLÍGONOS NO ESTÁ IMPLEMENTADO, AUNQUE LA
        #VARIABLE ESTÉ A True
        procesaMP = True
        #campo para las tablas con el número de esquinas entrantes y salientes
        campoNEENT = "NEENT"
        campoNESAL = "NESAL"
        campoNESQ  = ""
        #extensión y fichero de variables internas por defecto
        extDef = ".shp"
        fichOpDef = "opcionesPorDefecto.txt"
        #ruta del plugin de trabajo
        rutaPlugin = os.path.dirname(os.path.realpath(__file__))+sep
        #nombre del fichero de valores por defecto
        opDef = rutaPlugin+fichOpDef
        #valores por defecto de algunos campos
        ValoresPorDefecto(self,opDef)
        ########################################################################
        # Run the dialog event loop
        result = self.dlg.exec_()
        ########################################################################
        # See if OK was pressed
        if result == 1:
            #número de esquinas y elementos (edificios) total
            net = 0
            nelt = 0
            #opciones de trabajo y escritura del fichero de opciones por defecto
            te,tol,angMin,angMax,dp,sel,uf,fich,ext = ExtraeOpciones(self,
                                                                     extDef,
                                                                     opDef)
            #compruebo si no se ha pasado nombre de fichero
            if len(fich)==0:
                #lanzamos error
                QMessageBox.critical(None,"Error",
                                     u"No se ha pasado fichero de resultados")
                #salimos de la función
                return
            #pasamos los valores angulares a radianes
            angMin *= pi/180.0
            angMax *= pi/180.0
            #nombre del campo a añadir ala tabla de atributos
            if te==True:
                campoNESQ = campoNEENT
            else:
                campoNESQ = campoNESAL
            #selecciono la capa activa
            capa = self.iface.activeLayer()
            #compruebo si no hay capa activa
            if not capa:
                #lanzamos error
                QMessageBox.critical(None,"Error",u"No hay ninguna capa activa")
                #salimos de la función
                return
            #información de la capa de trabajo
            providerCapa = capa.dataProvider()
            #compruebo con qué elementos hay que trabajar
            if sel==True:
                #extraigo sólo los elementos seleccionados y los cuento
                elementos = capa.selectedFeatures()
                nElem = capa.selectedFeatureCount()
            else:
                #extraigo todos los elementos y los cuento
                elementos = capa.getFeatures()
                nElem = capa.featureCount()
            #compruebo si no hay elementos de trabajo
            if nElem==0:
                #lanzamos error
                QMessageBox.critical(None,"Error",
                                     u"No hay ningún elemento de trabajo")
                #salimos de la función
                return
            ####################################################################
            #sistema de referencia de la capa
            crs = providerCapa.crs()
            #campos de los elementos de la capa de trabajo
            camposCapa = providerCapa.fields()
            #añadimos el campo de número de esquinas encontradas
            camposCapa.append(QgsField(campoNESQ,QVariant.Int))
            #creo la capa de puntos interiores o experiores
            wPtos = QgsVectorFileWriter(fich+"_PTOS"+ext,"CP1250",camposCapa,
                                        QGis.WKBPoint,crs,"ESRI Shapefile")
            #creo la capa de polígonos simplificados, si hay posibilidad
            if dp==True:
                wDP = QgsVectorFileWriter(fich+"_DP"+ext,"CP1250",camposCapa,
                                          QGis.WKBPolygon,crs,"ESRI Shapefile")
            else:
                wDP = 0
            ####################################################################
            #creamos una barra de progreso y ponemos el máximo a 100
            qgis.utils.iface.messageBar().clearWidgets()
            progresoMessageBar = qgis.utils.iface.messageBar()
            progreso = QProgressBar()
            progreso.setMaximum(100)
            #pasamos la barra de progreso a la messageBar
            progresoMessageBar.pushWidget(progreso)
            ####################################################################
            #lista vacía y diccionario para los nombres de las nuevas capas
            nomCapas = []
            nomCapasD = {}
            #contador de elementos de trabajo
            cuentaElem = 0
            #recorro todos los elementos
            for elem in elementos:
                #extraigo la geometría y su tipo
                geom = elem.geometry()
                tipoGeom = geom.type()
                #compruebo si el tipo es polígono o multipolígono
                if tipoGeom==QGis.Polygon:
                    #tratamos el elemento como un polígono
                    poli = geom.asPolygon()
                    #lo procesamos
                    net,nelt = ProcesaElemento(camposCapa,crs,elem,poli,te,tol,
                                               angMin,angMax,dp,uf,fich,ext,net,
                                               nelt,wPtos,wDP,nomCapas,
                                               nomCapasD)
                #elif (procesaMP==True)and(tipoGeom==QGis.MultiPolygon):
                    ##¡POR EL MOMENTO, ESTA PARTE NO ESTÁ PROGRAMADA!
                ################################################################
                #aumento el contador de elementos de trabajo
                cuentaElem += 1
                #porcentaje de los elementos procesado
                porcentaje = (cuentaElem/float(nElem))*100.0
                #actualizo la barra de estado
                progreso.setValue(porcentaje)
            #supongo que esto limpia algo usado por la barra de progreso
            qgis.utils.iface.messageBar().clearWidgets()
            ####################################################################
            #recorro las capas de polígonos
            for i in range(0,len(nomCapas)):
                #las voy cerrado
                del nomCapasD[nomCapas[i]]
            #cierro la capa de puntos
            del wPtos
            #cierro la capa de Douglas-Peucker, si ha lugar
            if dp==True:
                del wDP
            #mensaje de número de esquinas entrantes encontradas
            if te==True:
                mensaje = u"Elementos con esquinas entrantes: "
            else:
                mensaje = u"Elementos con esquinas salientes: "
            QMessageBox.information(None,u"Información",
                                    mensaje+str(nelt)+
                                    u"\nNúmero total de esquinas: "+str(net))
        #salimos de la función
        return
################################################################################
##############################FUNCIONES AUXILIARES##############################
################################################################################
def ProcesaElemento(campos,crs,elem,poli,te,tol,angMin,angMax,dp,uf,fich,ext,
                    net,nelt,wPtos,wDP,nomCapas,nomCapasD):
    """
Propósito: Procesa un elemento de tipo polígono
Entradas:  - Campos de la capa de trabajo (campos=capa.dataProvider.fields()).
             Ha de tener añadido un campo de tipo entero al final
           - Definición del sistema de referencia de la capa de trabajo
           - Elemento (de una lista elementos=capa.getFeatures()) de trabajo
           - Objeto polígono, que proviene de elem.geometry.asPolygon()
           - Tipo de esquinas a buscar: True/False -> entrantes/salientes
           - Tolerancia lineal
           - Ángulo mínimo, en radianes
           - Ángulo máximo, en radianes
           - Identificador para simplificar con Douglas-Peucker
           - Identificador para guardar resultados en un único fichero
           - Nombre del fichero para salvar datos
           - Extensión del fichero para guardar datos
           - Número total de esquinas antes de la ejecución
           - Número de elementos total antes de la ejecución
           - Descriptor de fichero de puntos para las esquinas entrantes
           - Descriptor de fichero de polígonos simplificados
           - Lista con los nombres de las capas de trabajo. Al término de la
             ejecución de la función sale actualizada, si ha lugar
           - Diccionario con los descriptores de los ficheros de elementos (el
             índice son los nombres de los ficheros). Al término de la ejecución
             de la función sale actualizada, si ha lugar
Salidas:   - Número total de esquinas
           - Número de elementos total
Historia:  25 de octubre de 2014: Creación de la función
           22 de diciembre de 2014: Tenemos en cuenta el tipo de esquina
    """

    #si el elemento no tiene coordenadas, terminamos la ejecución
    if len(poli)<1:
        return (net,nelt)
    #coordenadas del primer ring, que es el borde exterior
    coor = poli[0]
    #calculo las esquinas
    nes,esq,coorEsq,polDP,esqDP = CalculaEsquinasPoligono(coor,te,tol,angMin,
                                                          angMax,dp)
    #aumentamos el contador de esquinas totales
    net += nes
    #aumentamos el contador de polígonos con esquinas detectadas
    if nes>0:
        nelt += 1
    #objeto para escribir elementos en una capa
    elemWriter = QgsFeature()
    #recorro los puntos detectados como esquinas
    for i in range(0,nes):
        #punto como elemento geométrico
        puntoGeom = QgsGeometry.fromPoint(QgsPoint(coorEsq[i]))
        #añado el atributo de número de esquinas entrantes
        atributos = elem.attributes()
        atributos.append(nes)
        #escribo el punto en la capa
        elemWriter.setGeometry(puntoGeom)
        elemWriter.setAttributes(atributos)
        wPtos.addFeature(elemWriter)
    #lista para las coordenadas del polígono simplificado
    coorPolDP = [[]]
    #esquinas del posible polígono simplificado
    neDP = len(polDP)
    #comprobamos si hay polígono simplificado (un elemento con sólo tres
    #vértices es una línea, por lo que no puede ser creada como polígono por
    #polGeom = QgsGeometry.fromPolygon(coorPolDP))
    if neDP>=4:
        #recorremos sus vértices
        for i in range(0,neDP):
            #creo una lista de QgsPoint con los vértices
            coorPolDP[0].append(QgsPoint(polDP[i]))
        #creo una geometría con el polígono simplificado
        polGeom = QgsGeometry.fromPolygon(coorPolDP)
        #añado el atributo de número de esquinas entrantes
        atributos = elem.attributes()
        atributos.append(nes)
        #escribo el polígono en el fichero
        elemWriter.setGeometry(polGeom)
        elemWriter.setAttributes(atributos)
        wDP.addFeature(elemWriter)
    #identificador de escritura de elemento y nombre de capa
    escribeElem = False
    ncap = ""
    #diferencio entre guardar datos en múltiples archivos o en fichero único
    if (uf==False)and(nee>0):
        #indico que hay que escribir el elemento
        escribeElem = True
        #nombre de la capa
        ncap = fich+"_"+str(nes)+ext
    elif uf==True:
        #indico que hay que escribir el elemento
        escribeElem = True
        #nombre de la capa
        ncap = fich+ext
    #compruebo si hay que escribir y no existe el nombre de la capa
    if (escribeElem==True)and(not(ncap in nomCapas)):
        #añado el nombre a la lista
        nomCapas.append(ncap)
        #la creo para escribir
        wElem = QgsVectorFileWriter(ncap,"CP1250",campos,QGis.WKBPolygon,crs,
                                    "ESRI Shapefile")
        #la guardo en el diccionario
        nomCapasD.update({ncap:wElem})
    #compruebo si hay que escribir el elemento
    if escribeElem==True:
        #lista para las coordenadas del polígono original
        coorPol = [[]]
        #recorremos sus vértices
        for i in range(0,len(coor)):
            #creo una lista de QgsPoint con los vértices
            coorPol[0].append(QgsPoint(coor[i]))
        #creo una geometría con el polígono
        polGeom = QgsGeometry.fromPolygon(coorPol)
        #extraigo la capa de trabajo del diccionario
        wElem = nomCapasD[ncap]
        #añado el atributo de número de esquinas
        atributos = elem.attributes()
        atributos.append(nes)
        #asigno la geometría y los atributos del polígono
        elemWriter.setGeometry(polGeom)
        elemWriter.setAttributes(atributos)
        #lo escribo en la capa de trabajo
        wElem.addFeature(elemWriter)
    #salimos de la función
    return (net,nelt)
################################################################################
def ValoresPorDefecto(self,opcionesPorDefecto):
    """
Propósito: Asigna opciones por defecto a algunos elementos
Entradas:  - El objeto desde donde se llama la función
           - Nombre del fichero de texto de posibles opciones por defecto
Historia:  24 de octubre de 2014: Creación de la función
           22 de diciembre de 2014: Tenemos en cuenta el tipo de esquina
    """

    #siempre desactivo la opción de guardar las selecciones usadas
    self.dlg.valDefectoBox.setChecked(False)
    #ponemos la línea de texto como sólo lectura
    self.dlg.lineFichero.setReadOnly(True)
    #desactivo los dos botones de tipo de esquina
    self.dlg.entrantesButton.setChecked(False)
    self.dlg.salientesButton.setChecked(False)
    #comprobamos si existe el fichero de opciones por defecto
    if os.path.isfile(opcionesPorDefecto)==True:
        #leo el contenido del fichero de opciones por defecto
        idf = open(opcionesPorDefecto,'r')
        lineas = idf.readlines()
        idf.close()
        #número de líneas
        nLineas = len(lineas)
        #sólo continuamos si hay 7 líneas o más
        if nLineas>=7:
            #quito los caracteres de fin de línea
            for i in range(0,len(lineas)):
                lineas[i] = lineas[i].strip()
            #asignamos los valores leídos
            if int(lineas[0]):
                self.dlg.entrantesButton.setChecked(True)
            else:
                self.dlg.salientesButton.setChecked(True)
            self.dlg.tolDistDoubleSpinBox.setValue(float(lineas[1]))
            self.dlg.tolAngDebajoDoubleSpinBox.setValue(float(lineas[2]))
            self.dlg.tolAngEncimaDoubleSpinBox.setValue(float(lineas[3]))
            self.dlg.dpBox.setChecked(int(lineas[4]))
            self.dlg.soloSelecBox.setChecked(int(lineas[5]))
            self.dlg.unSoloFicheroBox.setChecked(int(lineas[6]))
            #salimos de la función
            return
    #tipo de esquinas a extraer por defecto
    self.dlg.entrantesButton.setChecked(True)
    #tolerancia en distancia por defecto
    self.dlg.tolDistDoubleSpinBox.setValue(0.0)
    #tolerancia para los valores angulares mínimo y máximo
    self.dlg.tolAngDebajoDoubleSpinBox.setValue(20.0)
    self.dlg.tolAngEncimaDoubleSpinBox.setValue(20.0)
    #refinado con Douglas-Peucker
    self.dlg.dpBox.setChecked(False)
    #sólo elementos seleccionados
    self.dlg.soloSelecBox.setChecked(False)
    #resultados en un único fichero
    self.dlg.unSoloFicheroBox.setChecked(True)
    #salimos de la función
    return
################################################################################
def ExtraeOpciones(self,extension,fichOpDef):
    """
Propósito: Extrae las opciones del plugin y escribe el fichero de opciones por
           defecto, si ha lugar
Entradas:  - El objeto desde donde se llama la función
           - Extensión de ficheros por defecto
           - Nombre del fichero de texto de opciones opciones por defecto
Salidas:   - Tipo de esquinas a extraer: True/False -> entrantes/salientes
           - Tolerancia lineal
           - Ángulo mínimo, en grados sexagesimales (un ángulo entrante recto se
             considera que mide 90 grados; uno saliente, 180 grados)
           - Ángulo máximo, en grados sexagesimales (un ángulo entrante recto se
             considera que mide 90 grados; uno saliente, 180 grados)
           - Identificador para simplificar con Douglas-Peucker (sólo se
             devuelve activado si, además de haberlo activado en el interfaz
             gráfico, la tolerancia lineal es mayor que 0
           - Identificador para trabajar sólo con los elementos seleccionados
           - Identificador para guardar resultados en un único fichero
           - Nombre de los ficheros de salida sin extensión
           - Extensión de trabajo
Historia:  24 de octubre de 2014: Creación de la función
           22 de diciembre de 2014: Añadimos el tipo de esquina a extraer
    """

    #extraemos todos los elementos
    te = self.dlg.entrantesButton.isChecked()
    tol = self.dlg.tolDistDoubleSpinBox.value()
    tolAngDebajo = self.dlg.tolAngDebajoDoubleSpinBox.value()
    tolAngEncima = self.dlg.tolAngEncimaDoubleSpinBox.value()
    dp = self.dlg.dpBox.isChecked()
    sel = self.dlg.soloSelecBox.isChecked()
    uf = self.dlg.unSoloFicheroBox.isChecked()
    ruta = self.dlg.lineFichero.text()
    #creo los ángulos menor y mayor, en grados sexagesimales
    if te==True:
        angMin = 90.0-tolAngDebajo
        angMax = 90.0+tolAngEncima
    else:
        angMin = 270.0-tolAngDebajo
        angMax = 270.0+tolAngEncima
    #compruebo si no hay posibilidad de aplicar Douglas-Peucker
    if (dp==True)and(tol<=0.0):
        #no se aplica Douglas-Peucker, aunque se haya activado
        dp = False
    #comprobamos si hay que escribir el fichero
    if self.dlg.valDefectoBox.isChecked()==True:
        #compruebo los valores de las check box
        if te==True:
            teAux = 1
        else:
            teAux = 0
        if dp==True:
            dpAux = 1
        else:
            dpAux = 0
        if sel==True:
            selAux = 1
        else:
            selAux = 0
        if uf==True:
            ufAux = 1
        else:
            ufAux = 0
        #abrimos el fichero para escribir
        idf = open(fichOpDef,'w')
        #escribimos los valores por defecto
        idf.write(str("%d" % teAux+linesep))
        idf.write(str("%.4f" % tol+linesep))
        idf.write(str("%.4f" % tolAngDebajo+linesep))
        idf.write(str("%.4f" % tolAngEncima+linesep))
        idf.write(str("%d" % dpAux+linesep))
        idf.write(str("%d" % selAux+linesep))
        idf.write(str("%d" % ufAux+linesep))
        #cerramos el fichero de trabajo
        idf.close()
    #comprobamos si se ha pasado ruta para salvar resultados
    if len(ruta)>0:
        #extraemos el posible nombre del fichero pasado
        ruta = self.dlg.lineFichero.text()
        #dividimos el texto en ruta y nombre base
        directorio,fichero = os.path.split(ruta)
        #divido el fichero en nombre y extensión
        fichBase,fichExt = os.path.splitext(fichero)
        #construyo el nombre sin extensión
        fich = directorio+sep+fichBase
        #compruebo si el nombre contiene o no extensión
        if len(fichExt)==0:
            #extensión por defecto
            ext = extension
        else:
            #compruebo si la extensión es igual a la pasada por defecto
            if (fichExt==extension.lower())or(fichExt==extension.upper()):
                #la extensión es la pasada
                ext = fichExt
            else:
                #al nombre del fichero se le añade la extensión original
                fich += fichExt
                #la extensión es por defecto
                ext = extension
    else:
        #no se ha pasado nombre de fichero
        fich = ""
        ext = extension
    #salimos de la función
    return (te,tol,angMin,angMax,dp,sel,uf,fich,ext)
################################################################################
def SeleccionaFichero(self):
    """
Propósito: Selecciona un fichero con un objeto QFileDialog
Historia:  24 de octubre de 2014: Creación de la función
    """

    #extraigo el nombre del fichero
    fichero = QFileDialog.getSaveFileName(None,
                             u"Nombre genérico para los ficheros de resultados",
                             options=QFileDialog.DontUseNativeDialog)
    #asigno nombre de fichero, si ha lugar
    if fichero!="":
        self.dlg.lineFichero.setText(fichero)
    #salimos de la función
    return
