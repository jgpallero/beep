# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BuscaEsquinasEnPoligonos
                                 A QGIS plugin
 Detecta las esquinas interiores de un polígono
                             -------------------
        begin                : 2014-10-17
        copyright            : (C) 2014 by J.L.G. Pallero
        email                : jgpallero@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load BuscaEsquinasEnPoligonos class from file BuscaEsquinasEnPoligonos
    from buscaesquinasenpoligonos import BuscaEsquinasEnPoligonos
    return BuscaEsquinasEnPoligonos(iface)
