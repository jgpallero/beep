# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_buscaesquinasenpoligonos.ui'
#
# Created: Tue Dec 23 20:36:13 2014
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_BuscaEsquinasEnPoligonos(object):
    def setupUi(self, BuscaEsquinasEnPoligonos):
        BuscaEsquinasEnPoligonos.setObjectName(_fromUtf8("BuscaEsquinasEnPoligonos"))
        BuscaEsquinasEnPoligonos.resize(457, 369)
        self.buttonBox = QtGui.QDialogButtonBox(BuscaEsquinasEnPoligonos)
        self.buttonBox.setGeometry(QtCore.QRect(230, 220, 221, 71))
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.tolDistDoubleSpinBox = QtGui.QDoubleSpinBox(BuscaEsquinasEnPoligonos)
        self.tolDistDoubleSpinBox.setGeometry(QtCore.QRect(10, 80, 91, 31))
        self.tolDistDoubleSpinBox.setDecimals(4)
        self.tolDistDoubleSpinBox.setMinimum(0.0)
        self.tolDistDoubleSpinBox.setMaximum(1000000000.0)
        self.tolDistDoubleSpinBox.setSingleStep(0.5)
        self.tolDistDoubleSpinBox.setProperty("value", 0.0)
        self.tolDistDoubleSpinBox.setObjectName(_fromUtf8("tolDistDoubleSpinBox"))
        self.labelTolDist = QtGui.QLabel(BuscaEsquinasEnPoligonos)
        self.labelTolDist.setGeometry(QtCore.QRect(110, 80, 281, 31))
        self.labelTolDist.setObjectName(_fromUtf8("labelTolDist"))
        self.soloSelecBox = QtGui.QCheckBox(BuscaEsquinasEnPoligonos)
        self.soloSelecBox.setGeometry(QtCore.QRect(10, 240, 221, 21))
        self.soloSelecBox.setObjectName(_fromUtf8("soloSelecBox"))
        self.lineFichero = QtGui.QLineEdit(BuscaEsquinasEnPoligonos)
        self.lineFichero.setGeometry(QtCore.QRect(10, 330, 351, 31))
        self.lineFichero.setAcceptDrops(True)
        self.lineFichero.setObjectName(_fromUtf8("lineFichero"))
        self.dpBox = QtGui.QCheckBox(BuscaEsquinasEnPoligonos)
        self.dpBox.setGeometry(QtCore.QRect(10, 210, 221, 21))
        self.dpBox.setObjectName(_fromUtf8("dpBox"))
        self.tolAngDebajoDoubleSpinBox = QtGui.QDoubleSpinBox(BuscaEsquinasEnPoligonos)
        self.tolAngDebajoDoubleSpinBox.setGeometry(QtCore.QRect(10, 120, 91, 31))
        self.tolAngDebajoDoubleSpinBox.setDecimals(4)
        self.tolAngDebajoDoubleSpinBox.setMinimum(0.0)
        self.tolAngDebajoDoubleSpinBox.setMaximum(90.0)
        self.tolAngDebajoDoubleSpinBox.setSingleStep(1.0)
        self.tolAngDebajoDoubleSpinBox.setProperty("value", 0.0)
        self.tolAngDebajoDoubleSpinBox.setObjectName(_fromUtf8("tolAngDebajoDoubleSpinBox"))
        self.tolAngEncimaDoubleSpinBox = QtGui.QDoubleSpinBox(BuscaEsquinasEnPoligonos)
        self.tolAngEncimaDoubleSpinBox.setGeometry(QtCore.QRect(10, 160, 91, 31))
        self.tolAngEncimaDoubleSpinBox.setDecimals(4)
        self.tolAngEncimaDoubleSpinBox.setMinimum(0.0)
        self.tolAngEncimaDoubleSpinBox.setMaximum(90.0)
        self.tolAngEncimaDoubleSpinBox.setSingleStep(1.0)
        self.tolAngEncimaDoubleSpinBox.setProperty("value", 0.0)
        self.tolAngEncimaDoubleSpinBox.setObjectName(_fromUtf8("tolAngEncimaDoubleSpinBox"))
        self.labelTolAngDebajo = QtGui.QLabel(BuscaEsquinasEnPoligonos)
        self.labelTolAngDebajo.setGeometry(QtCore.QRect(110, 120, 341, 31))
        self.labelTolAngDebajo.setObjectName(_fromUtf8("labelTolAngDebajo"))
        self.labelTolAngEncima = QtGui.QLabel(BuscaEsquinasEnPoligonos)
        self.labelTolAngEncima.setGeometry(QtCore.QRect(110, 160, 341, 31))
        self.labelTolAngEncima.setObjectName(_fromUtf8("labelTolAngEncima"))
        self.unSoloFicheroBox = QtGui.QCheckBox(BuscaEsquinasEnPoligonos)
        self.unSoloFicheroBox.setGeometry(QtCore.QRect(10, 270, 211, 21))
        self.unSoloFicheroBox.setObjectName(_fromUtf8("unSoloFicheroBox"))
        self.pushRuta = QtGui.QPushButton(BuscaEsquinasEnPoligonos)
        self.pushRuta.setGeometry(QtCore.QRect(370, 330, 81, 31))
        self.pushRuta.setObjectName(_fromUtf8("pushRuta"))
        self.valDefectoBox = QtGui.QCheckBox(BuscaEsquinasEnPoligonos)
        self.valDefectoBox.setGeometry(QtCore.QRect(10, 300, 341, 21))
        self.valDefectoBox.setObjectName(_fromUtf8("valDefectoBox"))
        self.esquinasBox = QtGui.QGroupBox(BuscaEsquinasEnPoligonos)
        self.esquinasBox.setGeometry(QtCore.QRect(10, 10, 441, 61))
        self.esquinasBox.setObjectName(_fromUtf8("esquinasBox"))
        self.entrantesButton = QtGui.QRadioButton(self.esquinasBox)
        self.entrantesButton.setGeometry(QtCore.QRect(10, 30, 91, 21))
        self.entrantesButton.setObjectName(_fromUtf8("entrantesButton"))
        self.salientesButton = QtGui.QRadioButton(self.esquinasBox)
        self.salientesButton.setGeometry(QtCore.QRect(120, 30, 81, 21))
        self.salientesButton.setObjectName(_fromUtf8("salientesButton"))

        self.retranslateUi(BuscaEsquinasEnPoligonos)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), BuscaEsquinasEnPoligonos.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), BuscaEsquinasEnPoligonos.reject)
        QtCore.QMetaObject.connectSlotsByName(BuscaEsquinasEnPoligonos)

    def retranslateUi(self, BuscaEsquinasEnPoligonos):
        BuscaEsquinasEnPoligonos.setWindowTitle(_translate("BuscaEsquinasEnPoligonos", "BuscaEsquinasEnPoligonos", None))
        self.labelTolDist.setText(_translate("BuscaEsquinasEnPoligonos", "Tolerancia en distancia (unidades del mapa)", None))
        self.soloSelecBox.setText(_translate("BuscaEsquinasEnPoligonos", "Sólo elementos seleccionados", None))
        self.dpBox.setText(_translate("BuscaEsquinasEnPoligonos", "Refinado con Douglas-Peucker", None))
        self.labelTolAngDebajo.setText(_translate("BuscaEsquinasEnPoligonos", "Tolerancia por debajo del ángulo recto (sexagesimal)", None))
        self.labelTolAngEncima.setText(_translate("BuscaEsquinasEnPoligonos", "Tolerancia por encima del ángulo recto (sexagesimal)", None))
        self.unSoloFicheroBox.setText(_translate("BuscaEsquinasEnPoligonos", "Resultados en un solo fichero", None))
        self.pushRuta.setText(_translate("BuscaEsquinasEnPoligonos", "Fichero", None))
        self.valDefectoBox.setText(_translate("BuscaEsquinasEnPoligonos", "Guardar las selecciones como valores por defecto", None))
        self.esquinasBox.setTitle(_translate("BuscaEsquinasEnPoligonos", "Esquinas a buscar", None))
        self.entrantesButton.setText(_translate("BuscaEsquinasEnPoligonos", "Entrantes", None))
        self.salientesButton.setText(_translate("BuscaEsquinasEnPoligonos", "Salientes", None))

