# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BuscaEsquinasEnPoligonos
                                 A QGIS plugin
 Detecta las esquinas interiores de un polígono
 Funciones auxiliares para la simplificación de polígonos usando el algoritmo de
 Douglas-Peucker clásico

 Para realizar algunos cálculos de distancia se ha utilizado

 Konrad Ebisch (2002), A correction to the Douglas-Peucker line generalization
 algorithm, Computers and Geosciences, vol. 28, págs. 995 a 997

 Para el algoritmo de Kanani, las referencias son

 M. Dutter, 2007, Generalization of building footprints derived from high
 resolution remote sensing data
 http://publik.tuwien.ac.at/files/pub-geo_1952.pdf
 E. Kanani, 2000, Robust estimators for geodetic transformations and GIS
 http://www.igp-data.ethz.ch/berichte/Blaue_Berichte_PDF/70.pdf
                              -------------------
        begin                : 2014-10-22
        copyright            : (C) 2014 by J.L.G. Pallero
        email                : jgpallero@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
################################################################################
#importamos las funciones necesarias
from math import atan2,sin,cos,fabs,sqrt
################################################################################
def DouglasPeucker(coor,tol):
    """
Propósito: Simplifica una polilínea utilizando el algoritmo clásico de
           Douglas-Peucker
Entradas:  - Coordenadas de los vértices de la polilínea en una variable de tipo
             lista de pares de coordenadas (x,y)
           - Tolerancia para la simplificación, en las mismas unidades que las
             coordenadas
Salidas:   - Coordenadas de los vértices de la polilínea, en una variable de
             tipo lista de pares de coordenadas (x,y)
           - Vector de un número de elementos igual al número de vértices de la
             polilínea original, que almacena el valor 1 en las posiciones
             correspondientes a los vértices originales usados en la línea
             simplificado, y 0 en el resto
Nota: Las listas de coordenadas de entrada y salida pueden ser listas de tuplas
      [(x,y),...,(x,y)] o listas de listas [[x,y],...,[x,y]]
Historia:  22 de octubre de 2014: Creación de la función
    """

    #número de puntos de trabajo
    nPtos = len(coor)
    #comprobamos casos especiales
    if (nPtos<=2)or(tol<=0.0):
        #se copian todos los puntos
        for i in range(0,nPtos):
            simp.append(coor[i])
        #se usan todos los puntos
        usados = [1]*nPtos
        #salimos de la función
        return (simp,usados)
    #coordenadas de la línea simplificada y vector de puntos usados
    simp = [];
    usados = [0]*nPtos
    #función que realmente hace los cálculos (usados se pasa por referencia)
    CalculaDouglasPeucker(coor,tol,0,nPtos-1,usados)
    #recorremos el número de puntos de la polilínea original
    for i in range(0,nPtos):
        #comprobamos si el punto se ha usado
        if usados[i]!=0:
            #copiamos las coordenadas del punto
            simp.append(coor[i])
    #salimos de la función
    return (simp,usados)
################################################################################
def CalculaDouglasPeucker(coor,tol,posIni,posFin,usados):
    """
Propósito: Simplifica una polilínea utilizando el algoritmo clásico de
           Douglas-Peucker
Entradas:  - Coordenadas de los vértices de la polilínea en una variable de tipo
             lista de pares de coordenadas (x,y)
           - Tolerancia para la simplificación, en las mismas unidades que las
             coordenadas
           - Posición del punto inicial de la lista de coordenadas a partir del
             cual se simplificará. Este punto siempre estará contenido en la
             polilínea simplificada
           - Posición del punto final de la lista de coordenadas hasta el que se
             simplificará. Este punto siempre estará contenido en la polilínea
             simplificada
           - Vector de un número de elementos igual al número de vértices de la
             polilínea original pasado en 'coor', cuyos valores HAN DE SER
             inicialmente 0. Tras la ejecución de la función, se almacenará el
             valor 1 en las posiciones correspondientes a los vértices
             originales usados en la polilínea simplificado, y 0 en el resto
Nota 1: 'posIni' ha de ser un valor menor o igual que 'posFin', hecho que no se
        comprueba internamente
Nota 2: La lista de coordenadas de entrada puede ser una lista de tuplas
        [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Historia:  22 de octubre de 2014: Creación de la función
    """

    #los puntos inicial y final siempre se usan
    usados[posIni] = 1
    usados[posFin] = 1
    #sólo continuamos si los puntos extremos no están seguidos
    if posIni!=(posFin-1):
        #distancia máxima de los puntos al segmento formado por los extremos
        dist,pos = DouglasPeuckerDistMaxPlano(coor,posIni,posFin)
        #comprobamos si esa distancia está fuera de tolerancia
        if dist>tol:
            #aplicamos el algoritmo a la parte anterior al punto de trabajo
            CalculaDouglasPeucker(coor,tol,posIni,pos,usados)
            #aplicamos el algoritmo a la parte anterior al punto de trabajo
            CalculaDouglasPeucker(coor,tol,pos,posFin,usados)
    #salimos de la función
    return
################################################################################
def DouglasPeuckerDistMaxPlano(coor,posIni,posFin):
    """
Propósito: Calcula la distancia máxima en el plano entre una serie de puntos y
           un segmento
Entradas:  - Coordenadas de los vértices de la polilínea en una variable de tipo
             lista de pares de coordenadas (x,y)
           - Posición del punto inicial de la lista de coordenadas a partir del
             cual se realizarán los cálculos
           - Posición del punto final de la lista de coordenadas hasta el que se
             realizarán los cálculos
Salidas:   - Distancia del punto más lejano del conjunto (posIni+1,...,posFin-1)
             al segmento definido por posIni->posFin
           - Posición en la lista de coordenadas original del punto más lejano
Nota 1: 'posIni' ha de ser un valor menor o igual que 'posFin', hecho que no se
        comprueba internamente
Nota 2: Si 'posIni' y 'posFin' son contiguos, se devuelven los valores -1 y
        'posIni'
Nota 3: La lista de coordenadas de entrada puede ser una lista de tuplas
        [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Historia:  22 de octubre de 2014: Creación de la función
    """

    #comprobamos si los dos puntos están seguidos
    if posIni==(posFin-1):
        #la distancia devuelta es -1.0
        dist = -1.0
        #la posición que devolvemos es la del punto inicial
        pos = posIni
        #salimos de la función
        return (dist,pos)
    #variables de salida
    dist = 0
    pos = 0
    #coordenadas del primer punto de la base
    xIni = coor[posIni][0]
    yIni = coor[posIni][1]
    #coordenadas del segundo punto de la base, referidas al primero
    xFin = coor[posFin][0]-xIni
    yFin = coor[posFin][1]-yIni
    #calculamos la longitud de la base y el seno y el coseno de la rotación
    dx,sA,cA = DouglasPeuckerParamRotaBase(xFin,yFin)
    #recorremos los puntos entre los extremos (excluidos)
    for i in range(posIni+1,posFin):
        #coordenadas del punto de trabajo referidas al punto inicial de la base
        xP = coor[i][0]-xIni;
        yP = coor[i][1]-yIni;
        #calculamos la distancia del punto de trabajo al segmento base
        distAux = DouglasPeuckerDistMaxPlanoAux(dx,sA,cA,xP,yP)
        #comprobamos si la distancia calculada es mayor que la anterior
        if distAux>dist:
            #actualizamos la distancia máxima
            dist = distAux
            #guardamos la posición del punto
            pos = i
    #salimos de la función
    return (dist,pos)
################################################################################
def DouglasPeuckerParamRotaBase(xBase2RB1,yBase2RB1):
    """
Propósito: Calcula los parámetros de rotación para girar el sistema de
           coordenadas con origen en el punto inicial de la base y llevar el eje
           X a coincidir con ella, para su uso en el aligerado de polilíneas
           mediante el algoritmo de Douglas-Peucker
Entradas:  - Coordenada X del punto final de la base en el sistema de
             coordenadas original, reducida al punto inicial
           - Coordenada Y del punto final de la base en el sistema de
             coordenadas original, reducida al punto inicial
Salidas:   - Longitud de la base
           - Seno del ángulo de rotación para llevar el eje X del sistema de
             coordenadas (con origen en el punto inicial de la base) a coincidir
             con el segmento base
           - Coseno del ángulo de rotación para llevar el eje X del sistema de
             coordenadas (con origen en el punto inicial de la base) a coincidir
             con el segmento base
Historia:  22 de octubre de 2014: Creación de la función
    """

    #ángulo a rotar para llevar el eje X a coincidir con el segmento base
    alfa = atan2(yBase2RB1,xBase2RB1);
    #calculamos las razones trigonométricas del ángulo de rotación
    sAlfa = sin(alfa);
    cAlfa = cos(alfa);
    #la longitud del segmento base será el valor absoluto de la coordenada X del
    #extremo final del segmento base en el sistema de coordenadas rotado
    lonBase = fabs(cAlfa*xBase2RB1+sAlfa*yBase2RB1);
    #salimos de la función
    return (lonBase,sAlfa,cAlfa)
################################################################################
def DouglasPeuckerDistMaxPlanoAux(lonBase,sAlfa,cAlfa,xVertRB1,yVertRB1):
    """
Propósito: Calcula la distancia de un punto a un segmento AB para su uso en el
           aligerado de polilíneas mediante el algoritmo de Douglas-Peucker
Entradas:  - Longitud de la base
           - Seno del ángulo de rotación para llevar el eje X del sistema de
             coordenadas (con origen en el punto inicial de la base) a coincidir
             con el segmento base
           - Coseno del ángulo de rotación para llevar el eje X del sistema de
             coordenadas (con origen en el punto inicial de la base) a coincidir
             con el segmento base
           - Coordenada X del punto de trabajo en el sistema de coordenadas
             original, reducida al punto inicial de la base
           - Coordenada Y del punto de trabajo en el sistema de coordenadas
             original, reducida al punto inicial de la base
Salidas:   - Distancia del punto a la línea base
Historia:  22 de octubre de 2014: Creación de la función
    """

    #transformamos el vértice de trabajo al nuevo sistema de coordenadas
    xVert1 = cAlfa*xVertRB1+sAlfa*yVertRB1;
    yVert1 = -sAlfa*xVertRB1+cAlfa*yVertRB1;
    #comprobamos la posición del punto con respecto al segmento base
    if (xVert1>=0.0)and(xVert1<=lonBase):
        #el punto de trabajo está entre los extremos del segmento base
        #su distancia hasta él es el valor absoluto de su coordenada Y en el
        #sistema rotado
        dist = fabs(yVert1)
    elif xVert1<0.0:
        #el punto de trabajo está a la izquierda del punto inicial de la base,
        #luego su distancia hasta el segmento será la distancia hasta dicho
        #punto inicial
        dist = sqrt(xVert1**2+yVert1**2)
    else:
        #el punto de trabajo está a la derecha del punto final de la base, luego
        #su distancia hasta el segmento es la distancia hasta dicho punto final
        dist = sqrt((xVert1-lonBase)**2+yVert1**2)
    #salimos de la función
    return dist
################################################################################
def ReordenaPoligonoKanani(coor):
    """
Propósito: Reordena un polígono de tal forma que su nuevo vértice de inicio
           forme con el punto más alejado de él la mayor distancia entre
           cuanquier otro par de puntos que se pueda formar en la lista
Entradas:  - Coordenadas de los vértices de la polilínea en una variable de tipo
             lista de pares de coordenadas (x,y). Las coordenadas del primer
             punto HAN DE REPETIRSE AL FINAL DEL LISTADO. Al término de la
             ejecución de la función, la lista contiene el polígono reordenado
             (TAMBIÉN SE REPITE EL PRIMER PUNTO AL FINAL)
Nota 1: Las listas de coordenadas de entrada y salida pueden ser listas de
        tuplas [(x,y),...,(x,y)] o listas de listas [[x,y],...,[x,y]]
Nota 2: Se puede encontrar información sobre el algoritmo de Kanani en:
        M. Dutter, 2007, Generalization of building footprints derived from high
        resolution remote sensing data
        http://publik.tuwien.ac.at/files/pub-geo_1952.pdf
        E. Kanani, 2000, Robust estimators for geodetic transformations and GIS
        http://www.igp-data.ethz.ch/berichte/Blaue_Berichte_PDF/70.pdf
Historia:  25 de octubre de 2014: Creación de la función
    """

    #calculamos el punto más alejado del centro de gravedad
    posIni,xG,yG = PtoMasAlejadoCDG(coor)
    #sólo continuamos si la nueva posición no es la antigua
    if posIni!=0:
        #suprimimos el último punto, que está repetido
        del coor[len(coor)-1]
        #recorremos los puntos desde el inicio hasta la posición detectada (la
        #incluimos para que se repita el vértice al final
        for i in range(0,posIni+1):
            #vamos añadiendo los vértices al final
            coor.append(coor[i])
        #suprimimos los puntos copiados, excepto el de posIni
        del coor[0:posIni]
    #salimos de la función
    return
################################################################################
def PtoMasAlejadoCDG(coor):
    """
Propósito: Calcula la posición del punto de un polígono más alejado de su centro
           de gravedad
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y). El primer punto HA de
             repetirse al final
Salidas:   - Posición en la lista de entrada del punto más alejado del centro de
             gravedad del polígono
           - Coordenada X del centro de gravedad del polígono
           - Coordenada Y del centro de gravedad del polígono
Nota: La lista de coordenadas de entrada puede ser una lista de tuplas
      [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Historia:  25 de octubre de 2014: Creación de la función
    """

    #número de elementos del polígono
    nPtos = len(coor)
    #calculamos el centro de gravedad del polígono
    xG,yG = CentroGravedadPoligono(coor)
    #distancia y posición del punto más alejado del centro de gravedad
    pos = 0
    dist = -1.0
    #recorremos todos los puntos de trabajo, menos el último, que está repetido
    for i in range(0,nPtos-1):
        #calculo la distancia del centro de gravedad al punto de trabajo
        distAux = sqrt((coor[i][0]-xG)**2+(coor[i][1]-yG)**2)
        #compruebo si la distancia es mayor que la más grande calculada
        if distAux>dist:
            #ajusto la posición y la distancia
            pos = i
            dist = distAux
    #salimos de la función
    return (pos,xG,yG)
################################################################################
def CentroGravedadPoligono(coor):
    """
Propósito: Calcula el centro de gravedad de un polígono
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y). El primer punto HA de
             repetirse al final
Salidas:   - Coordenada X del centro de gravedad del polígono
           - Coordenada Y del centro de gravedad del polígono
Nota: La lista de coordenadas de entrada puede ser una lista de tuplas
      [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Historia:  25 de octubre de 2014: Creación de la función
    """

    #número de elementos del polígono
    nPtos = len(coor)
    #variables auxiliares
    xSum = 0.0
    ySum = 0.0
    #recorremos todos los puntos, menos el último, que está repetido
    for i in range(0,nPtos-1):
        #vamos sumando coordenadas
        xSum += coor[i][0]
        ySum += coor[i][1]
    #coordenadas del centro de gravedad
    xG = xSum/(nPtos-1)
    yG = ySum/(nPtos-1)
    #salimos de la función
    return (xG,yG)
