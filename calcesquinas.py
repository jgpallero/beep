# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BuscaEsquinasEnPoligonos
                                 A QGIS plugin
 Detecta las esquinas interiores de un polígono
 Funciones auxiliares para el cálculo de esquinas entrantes en un polígono
                              -------------------
        begin                : 2014-10-22
        copyright            : (C) 2014 by J.L.G. Pallero
        email                : jgpallero@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
################################################################################
#importamos las funciones necesarias
from math import atan2,sqrt,pi
from dpeucker import *
import copy
################################################################################
def CalculaEsquinasPoligono(coor,te,tolDist,angMin,angMax,simplifica):
    """
Proposito: Detecta las esquinas entrantes de un polígono
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y), donde el primer vértice HA DE
             REPETIRSE AL FINAL. Los vértices del polígono pueden estar
             ordenados de cualquier manera. Al término de la ejecución de la
             función, esta lista se devuelve ordenada en el sentido de las
             agujas del reloj y con el criterio de Kanani, por lo que puede que
             el vértice inicial no sea el mismo que en la entrada
           - Tipo de esquinas a detectar. Dos posibilidades:
             - True: Se calculan las esquinas entrantes del polígono
             - False: Se calculan las esquinas salientes del polígono
           - Tolerancia lineal, en las mismas unidades que las de las
             coordenadas. Cualquier vértice que tenga uno de sus lados de
             longitud menor que este valor no será tenido en cuenta
           - Ángulo mínimo, en radianes, por debajo del cual una esquina no será
             tenida en cuenta
           - Ángulo máximo, en radianes, por encima del cual una esquina no será
             tenida en cuenta
           - Identificador para simplificar un polígono original si hay alguna
             esquina que ha quedado fuera de tolerancia lineal. En ese caso, el
             polígono original se simplifica mediante el algoritmo de
             Douglas-Peucker, utilizando como parámetro de tolerancia la misma
             tolerancia lineal de trabajo, y se buscan sobre él las esquinas de
             trabajo. Dos posibilidades:
             - 0: No simplifico, aunque haya esquinas fuera de tolerancia lineal
             - Distinto de 0: Sí se simplifica
Salidas:   - Número de esquinas detectadas
           - Lista de listas con el mismo número de elementos que la lista de
             coordenadas original, que contiene la información referente a cada
             vértice. Cada sub lista contiene:
             - Pos. 0: Identificador de esquina. Dos posibilidades:
                       - True: Esquina válida, según el argumento 'te'
                       - False: Esquina no válida
             - Pos. 1: Ángulo en el vértice. Para esquinas entrantes el dominio
                       es [0,pi), mientras que para salientes será [pi,2*pi]
             - Pos. 2: Distancia al punto contiguo más cercano al vértice
           - Lista de tuplas con las coordenadas de las esquinas válidas
             detectadas
           - Lista de tuplas con las coordenadas de los vértices del polígono
             simplificado. Si no lo ha sido, la lista se devuelve vacía
           - Lista de listas con el mismo número de elementos que la lista de
             coordenadas del polígono simplificado (si lo ha sido), que contiene
             la información referente a cada vértice, en el mismo formato que el
             del segundo argumento de salida. Si el polígono no ha sido, la
             lista se devuelve vacía
Nota 1: La lista de coordenadas de entrada puede ser una lista de tuplas
        [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Nota 2: La lista de infomación de vértices salida es siempre una lista de listas
        [[id,ang,dist],...]
Nota 3: Si se ha tenido que simplificar el polígono original, los vértices
        correspondientes a las esquinas entrantes de salida pueden ser vértices
        de esquinas salientes en el polígono original y vivecersa, y así queda
        reflejado en la lista de información de vértices de la lista original
Nota 4: Las listas de coordenadas de las esquinas interiores y del polígono
        simplificado son siempre listas de tuplas [(x,y),...,(x,y)]
Nota 5: Esta función asume que angMin<=angMax
Historia:  23 de octubre de 2014: Creación de la función
           23 de diciembre de 2014: Tenemos en cuenta el tipo de esquina
    """

    #número de vértices del polígono
    nElem = len(coor)
    #fuerzo a que la ordenación de los vértices sea horaria
    OrdenPoligonoClockwise(coor)
    #reordenamos el polígono según el criterio de Kanani
    #esta operación no es necesaria si no hay que simplificar el polígono, pero
    #la hago igualmente para que el segundo argumento de salida sea congruente
    #con el listado de coordenadas en el caso en que sí que haya que simplificar
    ReordenaPoligonoKanani(coor)
    #calculamos las esquinas entrantes sin ningún tipo de constreñimiento
    nee,esq = EsquinasEntrantesPoligono(coor)
    #hago los cambios pertinentes según el tipo de esquina de trabajo
    nesq,esq = CambiosSegunTipoEsquina(te,nee,esq)
    #resto de valores de salida
    coorEsq = []
    esqDP = []
    polDP = []
    #comprobamos si hay que simplificar o no
    if (nesq>0)and(tolDist<=0.0):
        #reinicializamos el contador de esquinas
        nesq = 0
        #recorremos los vértices del polígono (menos el último, que es repetido)
        for i in range(0,nElem-1):
            #identificador de esquina y ángulo
            ide = esq[i][0]
            ang = esq[i][1]
            #comprobamos si estamos ante una esquina entrante o saliente válida
            if (ide==True)and(ang>=angMin)and(ang<=angMax):
                #aumento el contador de esquinas
                nesq += 1
                #copiamos las coordenadas
                coorEsq.append(coor[i])
            else:
                #indicamos que la esquina no es válida
                esq[i][0] = False
    elif (nesq>0)and(tolDist>0.0):
        #contador auxiliar de esquinas
        nesqAux = 0
        #recorremos los vértices del polígono (menos el último, que es repetido)
        for i in range(0,nElem-1):
            #identificador, ángulo de la esquina y distancia mínima
            ide = esq[i][0]
            ang = esq[i][1]
            dist = esq[i][2]
            #comprobamos si estamos ante una esquina tolerable
            if (ide==True)and(dist>=tolDist)and(ang>=angMin)and(ang<=angMax):
                #aumento el contador de esquinas
                nesqAux += 1
                #copiamos las coordenadas
                coorEsq.append(coor[i])
            else:
                #indicamos que la esquina no es válida
                esq[i][0] = False
        #comprobamos si hay que simplificar con Douglas-Peucker
        if (nesqAux!=nesq)and(simplifica!=0):
            #contador de número de esquinas y lista de coordenadas
            nesq = 0
            coorEsq = []
            #simplifico el polígono
            polDP,usadosDP = DouglasPeucker(coor,tolDist)
            #calculamos las esquinas entrantes del nuevo polígono
            neeDP,esqDP = EsquinasEntrantesPoligono(polDP)
            #hago los cambios pertinentes según el tipo de esquina de trabajo
            nesqDP,esqDP = CambiosSegunTipoEsquina(te,neeDP,esqDP)
            #comprobamos si se ha detectado alguna esquina entrante o saliente
            if nesqDP>0:
                #vértices del polígono simplificado (menos el último, por repe)
                for i in range(0,len(polDP)-1):
                    #identificador y ángulo de la esquina
                    ide = esqDP[i][0]
                    ang = esqDP[i][1]
                    #comprobamos si estamos ante una esquina tolerable
                    #ya no tenemos en cuenta la tolerancia lineal, porque hemos
                    #aplicado Douglas-Peucker
                    if (ide==True)and(ang>=angMin)and(ang<=angMax):
                        #aumento el contador de esquinas
                        nesq += 1
                        #copio las coordenadas de la lista aligerada
                        coorEsq.append(polDP[i])
                    else:
                        #indicamos que la esquina no es válida
                        esqDP[i][0] = False
        else:
            #el número de esquinas es el calculado con tolDist
            nesq = nesqAux
    #salimos de la función
    return (nesq,esq,coorEsq,polDP,esqDP)
################################################################################
def EsquinasEntrantesPoligono(coor):
    """
Proposito: Detecta las esquinas entrantes de un polígono
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y), donde el primer vértice HA de
             repetirse al final. LOS VÉRTICES EN EL POLÍGONO HAN DE ESTAR
             ORDENADOS EN EL SENTIDO DE LAS AGUJAS DEL RELOJ
Salidas:   - Número de esquinas entrantes detectadas
           - Lista de listas con el mismo número de elementos que la lista de
             coordenadas de entrada, que contiene la información referente a
             cada vértice del polígono. Cada sub lista contiene:
             - Pos. 0: Identificador de esquina entrante o saliente. Dos
                       posibilidades:
                       - True: Esquina entrante
                       - False: Esquina saliente
             - Pos. 1: Ángulo en el vértice. Para esquinas entrantes el dominio
                       es [0,pi), mientras que para salientes será [pi,2*pi]
             - Pos. 2: Distancia al punto contiguo más cercano al vértice
Nota 1: La lista de coordenadas de entrada puede ser una lista de tuplas
        [(x,y),...,(x,y)] o una lista de listas [[x,y],...,[x,y]]
Nota 2: La lista de salida es siempre una lista de listas [[id,ang,dist],...]
Historia:  23 de octubre de 2014: Creación de la función
    """

    #número de esquinas entrantes
    nee = 0
    #número de elementos de la lista
    nElem = len(coor)
    #copio el penúltimo punto al principio de la lista de coordenadas
    coor.insert(0,coor[nElem-2])
    #la lista contiene ahora un elemento más
    nElem += 1
    #lista de salida
    esquinas = [0]*nElem
    #recorro los elementos de la lista, excluyendo el primer y último puntos
    for i in range(1,nElem-1):
        #coordenadas de los puntos de trabajo
        xAnt = coor[i-1][0]
        yAnt = coor[i-1][1]
        xPto = coor[i][0]
        yPto = coor[i][1]
        xSig = coor[i+1][0]
        ySig = coor[i+1][1]
        #compruebo si estamos ante la primera vuelta del bucle
        if i==1:
            #el acimut al punto anterior y la distancia se calculan normalmente
            aciAnt = Acimut(xPto,yPto,xAnt,yAnt)
            distAnt = Dist2D(xPto,yPto,xAnt,yAnt)
        else:
            #el acimut anterior es el del siguiente en la vuelta anterior
            aciAnt = aciSig+pi
            #lo meto en dominio
            if aciAnt>2.0*pi:
                aciAnt -= 2.0*pi
            #la distancia es la calculada al punto siguiente en la vuelta previa
            distAnt = distSig
        #acimut y distancia al punto siguiente
        aciSig = Acimut(xPto,yPto,xSig,ySig)
        distSig = Dist2D(xPto,yPto,xSig,ySig)
        #calculo la diferencia en acimut anterior-siguiente
        dif = aciAnt-aciSig
        #la meto en dominio
        if dif<0.0:
            dif += 2.0*pi
        #como el polígono de entrada está ordenado en el sentido de las agujas
        #del reloj, las esquinas salientes estarán en el dominio [0,pi],
        #mientras que las entrantes lo estarán en [pi,2.0*pi]
        #resto el valor de la diferencia a 2*pi para que el ángulo de las
        #esquinas entrantes quede en el dominio [0,pi]
        dif = 2.0*pi-dif
        #distancia mínima a los puntos contiguos
        distMin = min(distAnt,distSig)
        #compruebo si la esquina es entrante y almaceno toda la información
        if dif<pi:
            #aumento el contador de esquinas entrantes
            nee += 1
            #lista con el identificador, el ángulo y la distancia mínima
            infoEsquina = [True,dif,distMin]
        else:
            #lista con el identificador, el ángulo y la distancia mínima
            infoEsquina = [False,dif,distMin]
        #añadimos la información a la variable de salida
        esquinas[i] = infoEsquina
    #quitamos el elemento añadido al inicio de la lista de entrada
    del coor[0]
    #también la lista de salida tiene un elemento de más al inicio
    del esquinas[0]
    #copiamos la información del primer punto en el último, que es el mismo y no
    #se ha recorrido en el bucle principal
    #es nElem-2 porque nElem almacena un elemento de más
    esquinas[nElem-2] = copy.deepcopy(esquinas[0])
    #devuelvo los resultados
    return (nee,esquinas)
################################################################################
def CambiosSegunTipoEsquina(te,nee,esquinas):
    """
Proposito: Cambia las esquinas entrantes detectadas por la función
           'EsquinasEntrantesPoligono' en esquinas salientes
Entradas:  - Tipo de esquinas a detectar. Dos posibilidades:
             - True: Se calculan las esquinas entrantes del polígono
             - False: Se calculan las esquinas salientes del polígono
           - Número de esquinas entrantes devuelto por la función
             'EsquinasEntrantesPoligono'
           - Lista de listas devuelta por la función 'EsquinasEntrantesPoligono'
Salidas:   - Número de esquinas de trabajo. Si 'te' vale True, número de
             esquinas entrantes, es decir, el valor del segundo argumento
             pasado. Si 'te' vale False, número de esquinas salientes
           - Lista de listas igual que la pasada como tercer argumento. Si 'te'
             vale True, el valor devuelto es idéntico al pasado como tercer
             argumento. Si 'te' vale False, el valor devuelto tiene cambiados
             los identificadores True por False en los elementos de la primera
             columna (ver la descripción de la variable en la ayuda de la
             función 'EsquinasEntrantesPoligono')
Nota: El cambio de esquinas entrantes por salientes SÓLO se realiza si el primer
      argumento de entrada vale False
Historia:  23 de diciembre de 2014: Creación de la función
    """

    #compruebo si hay que detectar esquinas entrantes o salientes
    if te==True:
        #número de esquinas de trabajo
        nesq = nee
    else:
        #número de esquinas salientes
        nesq = 0
        #número de elementos de la lista de esquinas
        nElem = len(esquinas)
        #cambio los identificadores de esquina
        for i in range(0,nElem):
            #voy cambiando el identificador de esquina
            esquinas[i][0] = not esquinas[i][0]
            #voy aumentando el contador de esquinas de trabajo
            if esquinas[i][0]==True:
                nesq += 1
        #compruebo si el primer y el último punto son True
        if (esquinas[0][0]==True)and(esquinas[nElem-1][0]==True):
            #he contado una esquina saliente de más
            nesq -= 1
    #devuelvo los resultados
    return (nesq,esquinas)
################################################################################
def OrdenPoligonoClockwise(coor):
    """
Proposito: Ordena los vértices de un polígono en el sentido de las agujas del
           reloj
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y), donde el primer vértice HA de
             repetirse al final. Al término de la ejecución de la función, si
             los vértices del polígono estaban ordenados en sentido antihorario
             se devuelve el vector de entrada con los vértices ordenados en el
             sentido de las agujas del reloj
Nota: La lista de coordenadas puede ser una lista de tuplas [(x,y),...,(x,y)] o
      una lista de listas [[x,y],...,[x,y]]
Historia:  22 de octubre de 2014: Creación de la función
    """

    #comprobamos si los vértices están ordenados en sentido antihorario
    if SuperficiePoligono(coor)>0.0:
        #cambiamos el orden de almacenamiento
        coor.reverse()
    #salimos de la función
    return
################################################################################
def SuperficiePoligono(coor):
    """
Proposito: Calcula la superficie de un polígono en el plano
Entradas:  - Coordenadas de los vértices del polígono en una variable de tipo
             lista de pares de coordenadas (x,y), donde el primer vértice HA de
             repetirse al final
Salidas:   - Superficie del polígono. Dos posibilidades:
             - Número negativo: los vértices del polígono están ordenados en el
               sentido de las agujas del reloj
             - Número positivo: los vértices del polígono están ordenados en
               sentido antihorario
Nota 1: El algoritmo utilizado es la segunda expresión de la ecuación 21.4.20
        del Numerical Recipes, tercera edición, página 1127
Nota 2: La lista de coordenadas puede ser una lista de tuplas [(x,y),...,(x,y)]
        o una lista de listas [[x,y],...,[x,y]]
Historia:  21 de octubre de 2014: Creación de la función
    """

    #variable de salida
    sup = 0.0;
    #dimensiones de la lista de vértices
    nElem = len(coor)
    #recorremos los vértices hasta el penúltimo
    for i in range(0,nElem-1):
        #vamos sumando las contribuciones de cada lado
        sup += (coor[i+1][0]+coor[i][0])*(coor[i+1][1]-coor[i][1])
    #el bucle anterior calculaba el doble de la superficie
    sup /= 2.0
    #salimos de la función
    return sup
################################################################################
def Acimut(x1,y1,x2,y2):
    """
Proposito: Calcula el acimut de una alineación en el plano
Entradas:  - Coordenada X del punto inicial
           - Coordenada Y del punto inicial
           - Coordenada X del punto final
           - Coordenada Y del punto final
Salidas:   Acimut, en radianes, de la alineación INICIAL->FINAL
Historia:  18 de octubre de 2014: Creación de la función
    """

    #calculo el acimut
    aci = atan2(x2-x1,y2-y1)
    #lo meto en dominio
    if aci<0.0:
        aci += 2.0*pi
    #salimos de la función
    return aci
################################################################################
def Dist2D(x1,y1,x2,y2):
    """
Proposito: Calcula la distancia entre dos puntos en el plano euclídeo
Entradas:  - Coordenada X del punto inicial
           - Coordenada Y del punto inicial
           - Coordenada X del punto final
           - Coordenada Y del punto final
Salidas:   Distancia entre los dos puntos
Historia:  18 de octubre de 2014: Creación de la función
    """

    #calculo la distancia y salimos de la función
    return sqrt((x2-x1)**2+(y2-y1)**2)
